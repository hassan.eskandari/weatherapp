import XCTest
import RxSwift
import RxBlocking
import RxTest
@testable import WeatherApp

final class MockingNetwork: Networking {
    func getItem(_ path: String, parameters: [String : Any]?) -> Observable<WeatherInformation> {
        if parameters!["id"] as! Int == sydneyId {
            return sydneyWeather
        } else if parameters!["id"] as! Int == melbourneId {
            return melbourneWeather
        } else if parameters!["id"] as! Int == noCityId {
            return noCityWeather
        }
        return Observable<WeatherInformation>.error(RxError.noElements)
    }
}

class WeatherApiTests: XCTestCase {

    let network = AnyNetworking(MockingNetwork())
    var api: WeatherApi!
    
    override func setUp() {
        api = WeatherApi(with: network, iconUrlFormat: "%@")
    }
    
    func testGetWeather() throws {
        let weatherInfo = try? api.getWeather(for: sydneyId)
            .toBlocking()
            .last()
        
        XCTAssertNotNil(weatherInfo)
    }
    
    func testGetWeathers() throws {
        let weatherInfo = try? api.getWeather(for: [melbourneId, sydneyId])
            .toBlocking()
            .last()
        
        XCTAssertNotNil(weatherInfo)
        XCTAssertEqual(2, weatherInfo?.count)
    }

    func testGetWeatherNoData() throws {
        let weatherInfo = try? api.getWeather(for: noCityId)
            .toBlocking()
            .last()
        
        XCTAssertNil(weatherInfo)
    }

    func testGetWeatherFails() throws {
        let weatherInfo = try? api.getWeather(for: -1)
            .toBlocking()
            .last()
        
        XCTAssertNil(weatherInfo)
    }

}
