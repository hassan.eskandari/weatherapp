import XCTest
import RxSwift
import RxCocoa
import RxTest
@testable import WeatherApp

final class TestWeatherApi: WeatherApiProvider {
    func getWeather(for cityId: Int) -> Observable<WeatherInformation> {
        return sydneyWeather
    }
    
    func getWeather(for cities: [Int]) -> Observable<[WeatherInformation]> {
        return Observable.zip([sydneyWeather, melbourneWeather])
    }
}

class HomeViewModelTests: XCTestCase {
    var viewModel: HomeViewModel!
    var scheduler: TestScheduler!
    var disposeBag: DisposeBag!

    
    override func setUp() {
        viewModel = HomeViewModel(weatherApi: TestWeatherApi())
        scheduler = TestScheduler(initialClock: 0)
        disposeBag = DisposeBag()
    }
    
    func testLoadingWeather() {
        let weather = scheduler.createObserver([HomeItemViewModel].self)
        
        let trigger = scheduler.createColdObservable([.next(10, ())])
            .asDriver { _ in Driver.empty() }

        let selection = scheduler.createColdObservable([Recorded<Event<IndexPath>>]())
            .asDriver { _ in Driver.empty() }

        let input = HomeViewModel.Input(trigger: trigger, selection: selection)
        
        let output = viewModel.transform(input: input)

        output.weathers.drive(weather).disposed(by: disposeBag)
        
        scheduler.start()

        let homeItems = weather.events.first?.value.element
        let sydney = try? sydneyHomeItem.toBlocking().first()
        let melbourne = try? melbourneHomeItem.toBlocking().first()

        XCTAssertNotNil(homeItems)
        XCTAssertEqual([sydney!, melbourne!], homeItems!)
    }
    
}
