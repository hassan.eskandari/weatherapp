import RxSwift
@testable import WeatherApp

let sydneyId = 4163971
let melbourneId = 2147714
let noCityId = 0

let sydneyWeatherFile = FileResource<WeatherInformation>(fileName: "sydney")
let melbourneWeatherFile = FileResource<WeatherInformation>(fileName: "melbourne")
let noCityWeatherFile = FileResource<WeatherInformation>(fileName: "nocity")

var sydneyWeather: Observable<WeatherInformation> {
    return DefaultFileResourceLoader().load(resource: sydneyWeatherFile)
}

var sydneyHomeItem: Observable<HomeItemViewModel> {
    return DefaultFileResourceLoader()
        .load(resource: sydneyWeatherFile)
        .map { HomeItemViewModel(weatherInformation: $0) }
}

var melbourneWeather: Observable<WeatherInformation> {
    return DefaultFileResourceLoader().load(resource: melbourneWeatherFile)
}

var melbourneHomeItem: Observable<HomeItemViewModel> {
    return DefaultFileResourceLoader()
        .load(resource: melbourneWeatherFile)
        .map { HomeItemViewModel(weatherInformation: $0) }
}

var noCityWeather: Observable<WeatherInformation> {
    return DefaultFileResourceLoader().load(resource: noCityWeatherFile)
}
