//
//  Network.swift
//  WeatherApp
//
//  Created by Hassan on 4/4/20.
//  Copyright © 2020 hassaneskandari. All rights reserved.
//

import Foundation
import Alamofire
import RxAlamofire
import RxSwift

protocol Networking {
    associatedtype DataType where DataType : Decodable
    
    func getItem(_ path: String, parameters: [String: Any]?) -> Observable<DataType>
}

class AnyNetworking<T: Decodable>: Networking {

    private let _getItem : (String, [String : Any]?) -> Observable<T>
    
    init<U: Networking>(_ networking: U) where U.DataType == T {
        _getItem = networking.getItem
    }
    
    func getItem(_ path: String, parameters: [String : Any]?) -> Observable<T> {
        return _getItem(path, parameters)
    }
}

final class Network<T: Decodable>: Networking {
    
    typealias DataType = T
    
    public let endPoint: String
    public let appId: String
    public let scheduler: ConcurrentDispatchQueueScheduler
    
    init(_ endPoint: String, appId: String) {
        self.endPoint = endPoint
        self.appId = appId
        
        scheduler = ConcurrentDispatchQueueScheduler(qos: DispatchQoS(qosClass: .background, relativePriority: 1))
    }
    
    private func getAbsolutePath(_ path: String) -> String {
        if path.starts(with: endPoint) {
            return path
        } else {
            return "\(endPoint)/\(path)"
        }
    }
    
    private func buildGet(_ path: String, parameters: [String: Any]?) -> Observable<Data> {
        let absolutePath = getAbsolutePath(path)
        var parameters = parameters ?? [String: Any]()
        parameters["appid"] = appId
        
        return RxAlamofire
            .data(.get, absolutePath, parameters: parameters)
            .debug()
            .observeOn(scheduler)
    }
    
    func getItem(_ path: String, parameters: [String: Any]?) -> Observable<DataType> {
        return buildGet(path, parameters: parameters)
            .map({ data -> DataType in
                return try JSONDecoder().decode(DataType.self, from: data)
            })
    }
    
}
