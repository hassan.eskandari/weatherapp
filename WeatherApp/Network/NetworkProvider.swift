import Foundation

final class NetworkProvider {
    private let endPoint = "https://api.openweathermap.org/data/2.5"
    private let iconUrlFormat = "http://openweathermap.org/img/wn/%@@2x.png"
    private let appId = "9e4a4fe1933eaaf26a367947020c9729"
    
    public func makeWeatherApi() -> WeatherApiProvider {
        let network = AnyNetworking(Network<WeatherInformation>(endPoint, appId: appId))
        return WeatherApi(with: network, iconUrlFormat: iconUrlFormat)
    }
}
