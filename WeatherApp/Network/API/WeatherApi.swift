import Foundation
import RxSwift

protocol WeatherApiProvider {
    func getWeather(for cityId: Int) -> Observable<WeatherInformation>
    func getWeather(for cities: [Int]) -> Observable<[WeatherInformation]>
}

final class WeatherApi: WeatherApiProvider {
    
    private let network: AnyNetworking<WeatherInformation>
    private let iconUrlFormat: String
    
    init(with network: AnyNetworking<WeatherInformation>, iconUrlFormat: String) {
        self.network = network
        self.iconUrlFormat = iconUrlFormat
    }
    
    func getWeather(for cityId: Int) -> Observable<WeatherInformation> {
        return network
            .getItem("weather", parameters: ["id" : cityId, "units" : "metric"])
            .map { wi in
                var wi = wi
                wi.weather = wi.weather.map { w in
                    var w = w
                    if let icon = w.icon {
                        w.icon = String(format: self.iconUrlFormat, icon)
                    }
                    return w
                }
                return wi
        }
    }
    
    func getWeather(for cities: [Int]) -> Observable<[WeatherInformation]> {
        return Observable.zip(cities.map { self.getWeather(for: $0) })
    }
    
}
