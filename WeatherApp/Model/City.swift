import Foundation

struct City : Codable {
    
    let id : Int
    let name : String
    let state : String?
    let country : String
    let coord : Coord?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case state = "state"
        case country = "country"
        case coord = "coord"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(Int.self, forKey: .id)
        name = try values.decode(String.self, forKey: .name)
        state = try values.decodeIfPresent(String.self, forKey: .state)
        country = try values.decode(String.self, forKey: .country)
        coord = try? Coord(from: decoder)
    }
    
}
