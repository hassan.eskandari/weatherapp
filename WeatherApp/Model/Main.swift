import Foundation

struct Main : Codable {
    
    let temp : Float
    let pressure : Int
    let humidity : Int
    let tempMin : Float
    let tempMax : Float
    
    enum CodingKeys: String, CodingKey {
        case temp = "temp"
        case pressure = "pressure"
        case humidity = "humidity"
        case tempMin = "temp_min"
        case tempMax = "temp_max"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        temp = try values.decode(Float.self, forKey: .temp)
        pressure = try values.decode(Int.self, forKey: .pressure)
        humidity = try values.decode(Int.self, forKey: .humidity)
        tempMin = try values.decode(Float.self, forKey: .tempMin)
        tempMax = try values.decode(Float.self, forKey: .tempMax)
    }
    
}
