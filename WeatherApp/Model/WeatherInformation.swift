import Foundation

struct WeatherInformation : Codable {
    
    let coord : Coord?
    var weather : [Weather]
    let base : String?
    let main : Main
    let visibility : Int?
    let wind : Wind?
    let clouds : Cloud?
    let dt : Int?
    let sys : Sys?
    let id : Int?
    let name : String
    let cod : Int?
    
    enum CodingKeys: String, CodingKey {
        case coord = "coord"
        case weather = "weather"
        case base = "base"
        case main = "main"
        case visibility = "visibility"
        case wind = "wind"
        case clouds = "clouds"
        case dt = "dt"
        case sys = "sys"
        case id = "id"
        case name = "name"
        case cod = "cod"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        coord = try values.decode(Coord.self, forKey: .coord)
        weather = try values.decode([Weather].self, forKey: .weather)
        base = try values.decodeIfPresent(String.self, forKey: .base)
        main = try values.decode(Main.self, forKey: .main)
        visibility = try values.decodeIfPresent(Int.self, forKey: .visibility)
        wind = try values.decode(Wind.self, forKey: .wind)
        clouds = try values.decode(Cloud.self, forKey: .clouds)
        dt = try values.decodeIfPresent(Int.self, forKey: .dt)
        sys = try values.decode(Sys.self, forKey: .sys)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decode(String.self, forKey: .name)
        cod = try values.decodeIfPresent(Int.self, forKey: .cod)
    }
    
}
