import Foundation
import RxSwift
import RxCocoa

class ActivityIndicator: SharedSequenceConvertibleType {
    public typealias Element = Bool
    public typealias SharingStrategy = DriverSharingStrategy
    
    private let lock = NSRecursiveLock()
    private let behavior = BehaviorRelay<Bool>(value: false)
    private let loading: SharedSequence<SharingStrategy, Element>
    
    init() {
        loading = behavior.asDriver().distinctUntilChanged()
    }
    
    func asSharedSequence() -> SharedSequence<DriverSharingStrategy, Element> {
        return loading
    }
    
    fileprivate func trackActivityOfObservable<O: ObservableConvertibleType>(_ source: O) -> Observable<O.Element> {
        
        return source.asObservable()
            .do(onNext: { _ in self.sendStopLoading() },
                onError: { _ in self.sendStopLoading() },
                onCompleted: sendStopLoading,
                onSubscribe: subscribed)
    }
    
    private func subscribed() {
        lock.lock()
        behavior.accept(true)
        lock.unlock()
    }
    
    private func sendStopLoading() {
        lock.lock()
        behavior.accept(false)
        lock.unlock()
    }
}

extension ObservableConvertibleType {
    func trackActivity(_ activityIndicator: ActivityIndicator) -> Observable<Element> {
        return activityIndicator.trackActivityOfObservable(self)
    }
}
