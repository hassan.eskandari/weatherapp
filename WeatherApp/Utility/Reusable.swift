import UIKit

protocol Reusable {
    static var reusableId: String { get }
}

extension Reusable {
    static var reusableId: String {
        return String(describing: self)
    }
}

extension UITableViewCell: Reusable { }
extension UIViewController: Reusable { }

extension UIStoryboard {
    func instantiateViewController<T>() -> T where T : UIViewController {
        guard let viewController = instantiateViewController(withIdentifier: T.reusableId) as? T else {
            fatalError()
        }
        return viewController
    }
}
