import Foundation
import RxSwift

struct FileResource<T> {
    let fileName: String
}

protocol FileResourceLoader {
    func load<T: Decodable>(resource: FileResource<T>) -> Observable<T>
}

final class DefaultFileResourceLoader: FileResourceLoader {

    func load<T: Decodable>(resource: FileResource<T>) -> Observable<T> {
        return Observable<T>.create { observer in
            if let url = Bundle.main.url(forResource: resource.fileName, withExtension: "json") {
                do {
                    let data = try Data(contentsOf: url)
                    let jsonData = try JSONDecoder().decode(T.self, from: data)
                    observer.on(.next(jsonData))
                    observer.on(.completed)
                } catch {
                    print("error:\(error)")
                    observer.on(.error(error))
                }
            } else {
                observer.on(.completed)
            }
            return Disposables.create()
        }
    }
}
