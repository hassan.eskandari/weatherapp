import Foundation
import UIKit

final class Application {
    static let shared = Application()
    
    public func configureMainInterface(in window: UIWindow) {
        
        if UserDefaults.standard.value(forKey: "Cities") == nil {
            UserDefaults.standard.set([4163971, 2147714, 2174003], forKey: "Cities")
            UserDefaults.standard.synchronize()
        }
        
        let networkProvider = NetworkProvider()
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: Application.self))
        let navigationController = UINavigationController()
        let navigator = DefaultHomeNavigator(storyboard: storyboard,
                                             navigationController: navigationController,
                                             services: networkProvider)
        
        window.rootViewController = navigationController
        
        navigator.toHome()
    }
}
