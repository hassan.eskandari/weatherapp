import UIKit

class AddCityCell: UITableViewCell {
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var countryCodeLabel: UILabel!
    
    func bind(_ viewModel: AddCityItemViewModel) {
        cityNameLabel.text = viewModel.cityName
        countryCodeLabel.text = viewModel.countryCode
    }
}
