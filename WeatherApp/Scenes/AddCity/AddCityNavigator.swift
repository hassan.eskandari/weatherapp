import UIKit

protocol AddCityNavigator {
    func toAddCity()
    func citySelected()
}

final class DefaultAddCityNavigator: AddCityNavigator {
    
    private let storyboard: UIStoryboard!
    private let navigationController: UINavigationController!
    
    init(storyboard: UIStoryboard,
         navigationController: UINavigationController) {
        self.storyboard = storyboard
        self.navigationController = navigationController
    }
    
    func toAddCity() {
        let viewController: AddCityViewController = storyboard.instantiateViewController()
        
        viewController.viewModel = AddCityViewModel(fileManager: DefaultFileResourceLoader())
        viewController.navigator = self
        
        navigationController.pushViewController(viewController, animated: true)
    }
    
    func citySelected() {
        navigationController.dismiss(animated: true, completion: nil)
    }
}
