import Foundation
import UIKit
import RxSwift
import RxCocoa

class AddCityViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private let disposeBag = DisposeBag()
    var viewModel: AddCityViewModel!
    var navigator: AddCityNavigator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupTableView()
        bindViewModel()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        navigator = nil
    }
    
    private func setupView() {
        title = "Add City"
    }
    
    private func setupTableView() {
        tableView.estimatedRowHeight = 78
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorInset = UIEdgeInsets(top: 0.0, left: 8.0, bottom: 0.0, right: 8.0)
        tableView.separatorColor = UIColor(red: 117/255.0, green: 117/255.0, blue: 117/255.0, alpha: 1.0)
        tableView.tableFooterView = UIView()
    }
    
    private func bindViewModel() {
        assert(viewModel != nil)
        
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .map{ _ in }
        
        let searchQuery = searchBar.rx.text.orEmpty.asObservable()
        let selection = tableView.rx.itemSelected.asDriver { _ in Driver.empty() }
        
        let input = AddCityViewModel.Input(load: viewWillAppear,
                                           search: searchQuery,
                                           selection: selection)
        
        let output = viewModel.transform(input)
        
        output.loading.drive(activityIndicator.rx.isAnimating)
            .disposed(by: disposeBag)
        output.loading.map { !$0 }.drive(activityIndicator.rx.isHidden)
            .disposed(by: disposeBag)
        
        output.cities.drive(tableView.rx.items(cellIdentifier: AddCityCell.reusableId, cellType: AddCityCell.self)) { tv, viewModel, cell in
            cell.bind(viewModel)
        }.disposed(by: disposeBag)
        
        output.selected
            .do(onNext: { [weak self] _ in self?.navigator?.citySelected() })
            .drive()
            .disposed(by: disposeBag)
        
    }
}
