import Foundation

final class AddCityItemViewModel {
    let id: Int
    let cityName: String
    let countryCode: String
    
    init(id: Int, cityName: String, countryCode: String) {
        self.id = id
        self.cityName = cityName
        self.countryCode = countryCode
    }
}
