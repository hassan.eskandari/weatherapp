import Foundation
import RxSwift
import RxCocoa

final class AddCityViewModel {
        
    private let fileManager: FileResourceLoader?
    private var localCityList = [City]()
    
    private let cityList = PublishSubject<[AddCityItemViewModel]>()
    private let loading = BehaviorRelay(value: true)
    
    private let disposeBag = DisposeBag()
    
    private let backgroundScheduler: SchedulerType
    
    struct Input {
        let load: Observable<Void>
        let search: Observable<String>
        let selection: Driver<IndexPath>
    }
    
    struct Output {
        let loading: Driver<Bool>
        let cities: Driver<[AddCityItemViewModel]>
        let selected: Driver<Bool>
    }
    
    init(fileManager: FileResourceLoader) {
        self.fileManager = fileManager
        self.backgroundScheduler = ConcurrentDispatchQueueScheduler(qos: .background)
    }
    
    func transform(_ input: Input) -> Output {

        input.load.subscribe(onNext: { [weak self] in
            guard let self = self else { return }
            
            self.loading.accept(true)
            self.fileManager?
                .load(resource: FileResource<[City]>(fileName: "citylist"))
                .subscribeOn(self.backgroundScheduler)
                .subscribe(onNext: { [weak self] cityList in
                    guard let self = self else { return }
                    
                    self.localCityList = cityList
                    self.cityList.onNext(cityList.toAddCityItemViewModel())
                    self.loading.accept(false)
                    
                    }, onError: { error in
                        print("onError: \(error)")
                }).disposed(by: self.disposeBag)
            }).disposed(by: disposeBag)
        
        input.search
            .observeOn(backgroundScheduler)
            .throttle(.milliseconds(300), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] search in
                guard let self = self else { return }
                
                self.loading.accept(true)
                
                var filteredCities = [City]()
                if search.isEmpty {
                    filteredCities = self.localCityList
                } else {
                    filteredCities = self.localCityList
                        .filter { ($0.name.range(of: search) != nil) || $0.id == Int(search) }
                }
                
                self.cityList.onNext(filteredCities.toAddCityItemViewModel())
                
                self.loading.accept(false)
                
            }).disposed(by: disposeBag)
        
        let cities = cityList.asDriver { _ in Driver.empty()}
        
        let selected = input.selection
            .withLatestFrom(cities) { [weak self] (indexPath, cityList) -> Bool in
                guard let self = self else { return false }
                
                return self.addCity(cityList[indexPath.row].id)
        }.asDriver { _ in Driver.empty() }
        
        return Output(loading: loading.asDriver(),
                      cities: cities,
                      selected: selected)
    }
    
    private func addCity(_ cityId: Int) -> Bool {
        var cities = UserDefaults.standard.value(forKey: "Cities") as? [Int] ?? [Int]()
        
        guard cities.contains(cityId) == false else { return false }
        
        cities.append(cityId)
        
        UserDefaults.standard.set(cities, forKey: "Cities")
        UserDefaults.standard.synchronize()
        
        return true
    }
}

extension Array where Element == City {
    fileprivate func toAddCityItemViewModel() -> [AddCityItemViewModel] {
        return map { AddCityItemViewModel(id: $0.id, cityName: $0.name, countryCode: $0.country) }
    }
}
