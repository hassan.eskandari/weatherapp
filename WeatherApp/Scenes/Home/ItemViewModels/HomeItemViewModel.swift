import Foundation

final class HomeItemViewModel {
    let city: String
    let temperature: Float
    let icon: String
    let weatherInformation: WeatherInformation
    
    init(weatherInformation: WeatherInformation) {
        self.weatherInformation = weatherInformation
        self.city = weatherInformation.name
        self.temperature = weatherInformation.main.temp
        self.icon = weatherInformation.weather.first?.icon ?? ""
    }
}

extension HomeItemViewModel: Equatable {
    static func == (lhs: HomeItemViewModel, rhs: HomeItemViewModel) -> Bool {
        return lhs.city == rhs.city
            && lhs.temperature == rhs.temperature
            && lhs.icon == rhs.icon
    }
}
