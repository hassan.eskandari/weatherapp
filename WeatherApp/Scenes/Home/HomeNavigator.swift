import Foundation
import UIKit

protocol HomeNavigator {
    func toHome()
    func toAddCity()
}

class DefaultHomeNavigator: HomeNavigator {
    
    private let storyboard: UIStoryboard!
    private let navigationController: UINavigationController!
    private let services: NetworkProvider!
    
    init(storyboard: UIStoryboard,
         navigationController: UINavigationController,
         services: NetworkProvider) {
        self.storyboard = storyboard
        self.navigationController = navigationController
        self.services = services
    }
    
    func toHome() {
        let viewController: HomeViewController = storyboard.instantiateViewController()
        viewController.viewModel = HomeViewModel(weatherApi: NetworkProvider().makeWeatherApi())
        viewController.navigator = self
        
        navigationController.pushViewController(viewController, animated: true)
    }
    
    func toAddCity() {
        let navigationController = UINavigationController()
        let addCityNavigator = DefaultAddCityNavigator(storyboard: storyboard, navigationController: navigationController)
        
        addCityNavigator.toAddCity()
        self.navigationController.present(navigationController, animated: true, completion: nil)
    }
}
