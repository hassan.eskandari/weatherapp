import UIKit
import Kingfisher

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    func bind(_ viewModel: HomeItemViewModel) {
        self.cityNameLabel.text = viewModel.city
        self.temperatureLabel.text = "\(Int(viewModel.temperature))°"
        self.iconImageView.kf.setImage(with: URL(string: viewModel.icon))
    }
    
}
