import UIKit
import RxSwift
import RxCocoa

class HomeViewController: UIViewController {

    private let disposeBag = DisposeBag()
    var viewModel: HomeViewModel!
    var navigator: HomeNavigator!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addButton: UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupTableView()
        setupAddButton()
        bindViewModel()
    }

    private func setupView() {
        title = "Your Cities"
    }
    
    private func setupTableView() {
        tableView.refreshControl = UIRefreshControl()
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorInset = UIEdgeInsets(top: 0.0, left: 8.0, bottom: 0.0, right: 8.0)
        tableView.separatorColor = UIColor(red: 117/255.0, green: 117/255.0, blue: 117/255.0, alpha: 1.0)
        tableView.tableFooterView = UIView()
    }
    
    private func setupAddButton() {
        addButton.target = self
        addButton.action = #selector(addCity)
    }
    
    @objc func addCity() {
        navigator.toAddCity()
    }
    
    private func bindViewModel() {
        assert(viewModel != nil)

        let timer = Observable<Int>.timer(.seconds(0), period: .seconds(60), scheduler: MainScheduler.instance)
            .map { _ in }
            .asDriver { _ in Driver.empty() }
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .map{ _ in }
            .asDriver { _ in Driver.empty() }
        let pulltoRefresh = tableView.refreshControl!.rx
            .controlEvent(.valueChanged)
            .asDriver()

        let input = HomeViewModel.Input(trigger: Driver.merge(viewWillAppear, pulltoRefresh, timer),
                                         selection: tableView.rx.itemSelected.asDriver())

        let output = viewModel.transform(input: input)
        
        output.weathers.drive(tableView.rx.items(cellIdentifier: HomeTableViewCell.reusableId, cellType: HomeTableViewCell.self)) { tv, viewModel, cell in
                cell.bind(viewModel)
        }.disposed(by: disposeBag)
        
        output.loading
            .drive(tableView.refreshControl!.rx.isRefreshing)
            .disposed(by: disposeBag)
        
    }
}

