import Foundation
import RxSwift
import RxCocoa

final class HomeViewModel {
    
    private let weatherApi: WeatherApiProvider
    
    struct Input {
        let trigger: Driver<Void>
        let selection: Driver<IndexPath>
    }
    
    struct Output {
        let loading: Driver<Bool>
        let weathers: Driver<[HomeItemViewModel]>
    }
    
    init(weatherApi: WeatherApiProvider) {
        self.weatherApi = weatherApi
    }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        
        let weathers = input.trigger.flatMapLatest {
            return UserDefaults.standard.rx
                .observe([Int].self, "Cities")
                .filter { $0 != nil }
                .flatMapLatest { self.weatherApi.getWeather(for: $0!) }
                .trackActivity(activityIndicator)
                .asDriver { _ in Driver.empty() }
                .map { $0.map { HomeItemViewModel(weatherInformation: $0) } }
        }
        
        let loading = activityIndicator.asDriver()
        
        return Output(loading: loading, weathers: weathers)
    }
}
